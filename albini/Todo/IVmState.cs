namespace Todo
{
    /// <summary>
    /// This interface contains the methods used by microinstructions to alter the
    /// internal state of the VM. Implementations of it are only available to
    /// microinstructions, and it's not supposed to be used outside of the VM.
    /// </summary>
    public interface IVmState
    {
        /// <summary>Get the value of an internal VM register.</summary>
        ///
        /// <param name="register">The name of the register</param>
        /// <returns>The value contained in the register</returns>
        Value GetRegister(Register register);

        /// <summary>Set the value of an internal VM register.</summary>
        ///
        /// <param name="register">The name of the register</param>
        /// <param name="newValue">The value to set</param>
        void SetRegister(Register register, Value newValue);

        /// <summary>Get the next value in the input</summary>
        /// <returns>The next value if present, or an empty value otherwise</returns>
        Value GetInput();

        /// <summary>Add a value to the output</summary>
        /// <param name="newValue">The value to add</param>
        void AddOutput(Value newValue);

        /// <summary>Terminate the program</summary>
        void EndProgram();
    }
}
