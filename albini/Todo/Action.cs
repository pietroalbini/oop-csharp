namespace Todo
{
    /// <summary>This enum represents an action executed by the VM</summary>
    public enum Action
    {
        /// <summary>A value was picked from the input belt.</summary>
        PICK_INPUT,
        /// <summary>
        /// The value currently held in the main hand was dropped.
        /// </summary>
        DROP_MAIN_HAND,
        /// <summary>
        /// The value currently held in the main hand was added to the output.
        /// </summary>
        DROP_OUTPUT
    }
}
