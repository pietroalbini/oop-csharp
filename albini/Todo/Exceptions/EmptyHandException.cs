using System;

namespace Todo.Exceptions
{
    /// <summary>
    /// This exception is thrown when the hand is empty and the executed
    /// instruction requires a value in it.
    /// </summary>
    public class EmptyHandException : Exception
    {
        /// <inheritdoc />
        public override String ToString()
        {
            return "The hand is currently empty!";
        }
    }
}
