using System;

namespace Todo.Exceptions
{
    /// <summary>
    /// This exception is thrown when a program enters an infinite loop.
    /// </summary>
    public class InfiniteLoopException : Exception
    {
        /// <inheritdoc />
        public override String ToString()
        {
            return "The program entered an infinite loop!";
        }
    }
}

