using System.Collections.Generic;

namespace Todo.Instructions
{
    /// <summary>
    /// This interface represents an instruction executable by the VM
    /// </summary>
    public interface IInstruction
    {
        /// <summary>The unique ID of this instruction</summary>
        IUniqueId Id { get; }

        /// <summary>This method executes the instruction</summary>
        /// <params name="state">The state of the VM executing the instruction</params>
        /// <params name="code">The code currently being executed by the VM</params>
        /// <returns>The actions executed by the instruction</returns>
        IEnumerable<Action> Execute(IVmState state, IVmCode code);
    }
}
