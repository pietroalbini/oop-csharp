using System.Collections.Generic;
using System.Linq;

namespace Todo.Instructions
{
    /// <summary>
    /// This class implements the shared jump logic used by multiple jump
    /// instructions.
    /// </summary>
    public abstract class BaseJump : IInstruction
    {
        /// <summary>The target of this jump</summary>
        public IInstruction Target { get; private set; }

        /// <inheritdoc />
        public IUniqueId Id { get; private set; }

        /// <summary>Create a new instance of this jump</summary>
        public BaseJump()
        {
            this.Target = new JumpTarget();
            this.Id = UniqueIdGenerator.Instance.Next;
        }

        /// <inheritdoc />
        public IEnumerable<Action> Execute(IVmState state, IVmCode code)
        {
            Value current = state.GetRegister(Register.MAIN_HAND);
            if (Decide(current))
            {
                var dest = code.InstructionIndex(this.Target);
                state.SetRegister(Register.PROGRAM_COUNTER, Value.CreateNumber(dest));
            }
            return Enumerable.Empty<Action>();
        }

        /// <summary>Decide whether to jump or not based on the hand content.</summary>
        protected abstract bool Decide(Value current);
    }

    internal class JumpTarget : IInstruction
    {
        /// <inheritdoc />
        public IUniqueId Id { get; private set; }

        internal JumpTarget()
        {
            this.Id = UniqueIdGenerator.Instance.Next;
        }

        /// <inheritdoc />
        public IEnumerable<Action> Execute(IVmState state, IVmCode code)
        {
            return Enumerable.Empty<Action>();
        }
    }
}
