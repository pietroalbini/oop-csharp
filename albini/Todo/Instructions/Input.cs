using System.Collections.Generic;

namespace Todo.Instructions
{
    /// <summary>
    /// Pick the next value from the input data and put it into the main hand. If there are no
    /// more values to pick the program will terminate.
    /// </summary>
    public class Input : IInstruction
    {
        /// <inheritdoc />
        public IUniqueId Id { get; private set; }

        /// <summary>Create a new instance of the instruction</summary>
        public Input()
        {
            this.Id = UniqueIdGenerator.Instance.Next;
        }

        /// <inheritdoc />
        public IEnumerable<Action> Execute(IVmState state, IVmCode code)
        {
            // Ensure the hand is clean before picking an input
            // This is done explicitly to allow the view to animate the instruction properly.
            if (state.GetRegister(Register.MAIN_HAND).Present)
            {
                state.SetRegister(Register.MAIN_HAND, Value.CreateEmpty());
                yield return Action.DROP_MAIN_HAND;
            }

            Value next = state.GetInput();
            if (next.Present)
            {
                state.SetRegister(Register.MAIN_HAND, next);
                yield return Action.PICK_INPUT;
            }
            else
            {
                state.EndProgram();
            }
        }
    }
}
