using System.Collections.Generic;
using Todo.Exceptions;

namespace Todo.Instructions
{
    /// <summary>
    /// Put the value currently in the hand in the output and then remove it
    /// from the hand. If the value is missing throw an exception.
    /// </summary>
    public class Output : IInstruction
    {
        /// <inheritdoc />
        public IUniqueId Id { get; private set; }

        /// <summary>Create a new instance of the instruction</summary>
        public Output()
        {
            this.Id = UniqueIdGenerator.Instance.Next;
        }

        /// <inheritdoc />
        public IEnumerable<Action> Execute(IVmState state, IVmCode code)
        {
            Value current = state.GetRegister(Register.MAIN_HAND);
            if (!current.Present)
            {
                throw new EmptyHandException();
            }
            state.AddOutput(current);
            state.SetRegister(Register.MAIN_HAND, Value.CreateEmpty());
            yield return Action.DROP_OUTPUT;
        }
    }
}
