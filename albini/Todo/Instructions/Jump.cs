namespace Todo.Instructions
{
    /// <summary>
    /// This instruction always jumps to the target.
    /// </summary>
    public class Jump : BaseJump
    {
        /// <inheritdoc />
        protected override bool Decide(Value current)
        {
            return true;
        }
    }
}
