namespace Todo.Instructions
{
    /// <summary>
    /// This instruction jumps to the target if the value in the hand is zero.
    /// </summary>
    public class JumpZero : BaseJump
    {
        /// <inheritdoc />
        protected override bool Decide(Value current)
        {
            return current.Present && current.AsNumber == 0;
        }
    }
}

