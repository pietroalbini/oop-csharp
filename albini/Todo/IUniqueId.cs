namespace Todo
{
    /// <summary>
    /// This interface represents an opaque identifier which is unique across the
    /// execution of the program. Consumers of this ID shouldn't be able to get its
    /// inner value, which is left as an implementation detail.
    /// </summary>
    public interface IUniqueId
    {
        // Nothing here.
    }
}
