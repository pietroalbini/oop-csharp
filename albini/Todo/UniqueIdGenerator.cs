namespace Todo
{
    /// <summary>
    /// This class generates identifiers unique across the execution of the program.
    /// </summary>
    public class UniqueIdGenerator
    {
        /// <summary>Get the instance of this singleton</summary>
        public static UniqueIdGenerator Instance => instance;
        private static UniqueIdGenerator instance = new UniqueIdGenerator();

        private int counter;

        private UniqueIdGenerator()
        {
            this.counter = 0;
        }

        /// <summary>Get the next unique identifier</summary>
        public IUniqueId Next
        {
            get { return new UniqueId(this.counter++); }
        }

        private class UniqueId : IUniqueId
        {
            private readonly int id;

            internal UniqueId(int id)
            {
                this.id = id;
            }

            public override bool Equals(object other)
            {
                var cast = other as UniqueId;
                return cast != null && this.id == cast.id;
            }

            public override int GetHashCode()
            {
                return this.id;
            }
        }
    }
}
