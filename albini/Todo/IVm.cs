using System.Collections.Generic;

namespace Todo
{
    /// <summary>
    /// The Todo VM executes the provided instructions, returning the output
    /// </summary>
    public interface IVm
    {
        /// <summary>Check if the VM has finished execution</summary>
        bool Finished { get; }

        /// <summary>
        /// Get the list of values stored by the program on the output. The method can be
        /// called at any time during the execution, but if it's called before the end of
        /// the program it will not return the final results.
        /// </summary>
        IReadOnlyList<Value> Output { get; }

        /// <summary>
        /// Execute the program until it finishes. If the execution has already started
        /// this method will *not* restart the execution from scratch, but it will resume
        /// it from where it stopped.
        /// </summary>
        void Execute();

        /// <summary>
        /// Execute the program until an Action is done, then return that action.
        /// Multiple instructions can be executed before an Action is done, but an
        /// instruction can also yield more than one Action.
        /// </summary>
        /// <returns>The executed action or null if the execution ended</returns>
        Action? Step();
    }
}
