using Todo.Instructions;
using System.Collections.Generic;

namespace Todo
{
    /// <inheritdoc />
    public class VmCode : IVmCode
    {
        private List<IInstruction> code;
        private Dictionary<IUniqueId, int> id2idx;

        /// <inheritdoc />
        public int Size
        {
            get { return this.code.Count; }
        }

        /// <inheritdoc />
        public IInstruction this[int idx]
        {
            get { return this.code[idx]; }
        }

        /// <summary>Create a new instance of the code storage.</summary>
        public VmCode()
        {
            this.code = new List<IInstruction>();
            this.id2idx = new Dictionary<IUniqueId, int>();
        }

        /// <summary>Add a new instruction to the code storage.</summary>
        public void Add(IInstruction instr)
        {
            var idx = this.code.Count;
            this.code.Add(instr);
            this.id2idx[instr.Id] = idx;
        }

        /// <inheritdoc />
        public int InstructionIndex(IInstruction instr)
        {
            return this.id2idx[instr.Id];
        }
    }
}
