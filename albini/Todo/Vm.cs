using System.Collections.Generic;
using System.Linq;
using Todo.Exceptions;
using Todo.Instructions;

namespace Todo
{
    /// <inheritdoc />
    public class Vm : IVm
    {
        private const int LOOP_DETECTION_THRESHOLD = 100000;

        private VmCode code;
        private VmState state;
        private IEnumerator<Action> currentlyExecuting;

        /// <inheritdoc />
        public IReadOnlyList<Value> Output
        {
            get { return this.state.output; }
        }

        /// <inheritdoc />
        public bool Finished
        {
            get { return this.state.manuallyEnded || this.ProgramCounter >= this.code.Size; }
        }

        private int ProgramCounter
        {
            get { return this.state.GetRegister(Register.PROGRAM_COUNTER).AsNumber; }
            set { this.state.SetRegister(Register.PROGRAM_COUNTER, Value.CreateNumber(value)); }
        }

        /// <summary>Create a new instance of the VM</summary>
        /// <param name="program">The list of instructions the VM should /// execute</param>
        /// <param name="input">The list of values to treat as input</param>
        public Vm(IList<IInstruction> program, IList<Value> input)
        {
            this.state = new VmState(input);
            this.code = new VmCode();
            this.currentlyExecuting = Enumerable.Empty<Action>().GetEnumerator();
            foreach (var instr in program)
            {
                this.code.Add(instr);
            }
        }

        /// <inheritdoc />
        public void Execute()
        {
            while (Step() != null) {}
        }

        /// <inheritdoc />
        public Action? Step()
        {
            for (var i = 0; i < LOOP_DETECTION_THRESHOLD; i++)
            {
                // If we're in the middle of an instruction continue executing
                // it instead of fetching the next one.
                while (this.currentlyExecuting.MoveNext())
                {
                    return this.currentlyExecuting.Current;
                }

                if (this.Finished)
                {
                    return null;
                }
                var instr = this.code[this.ProgramCounter];
                // The program counter is incremented before executing the instruction because
                // the instruction can override it (for example with a jump).
                this.ProgramCounter++;
                this.currentlyExecuting = instr.Execute(this.state, this.code).GetEnumerator();
            }
            throw new InfiniteLoopException();
        }

        private class VmState : IVmState
        {
            public bool manuallyEnded;
            private Queue<Value> input;
            public List<Value> output;
            private Dictionary<Register, Value> registers;

            public VmState(IList<Value> input)
            {
                this.manuallyEnded = false;
                this.input = new Queue<Value>(input);
                this.output = new List<Value>();
                this.registers = new Dictionary<Register, Value>();
                this.registers[Register.MAIN_HAND] = Value.CreateEmpty();
                this.registers[Register.PROGRAM_COUNTER] = Value.CreateNumber(0);
            }

            /// <inheritdoc />
            public Value GetRegister(Register register)
            {
                return this.registers.ContainsKey(register) ? this.registers[register] : Value.CreateEmpty();
            }

            /// <inheritdoc />
            public void SetRegister(Register register, Value newValue)
            {
                this.registers[register] = newValue;
            }

            /// <inheritdoc />
            public Value GetInput()
            {
                return this.input.Count > 0 ? this.input.Dequeue() : Value.CreateEmpty();
            }

            /// <inheritdoc />
            public void AddOutput(Value newValue)
            {
                this.output.Add(newValue);
            }

            /// <inheritdoc />
            public void EndProgram()
            {
                this.manuallyEnded = true;
            }
        }
    }
}
