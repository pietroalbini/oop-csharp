namespace Todo
{
    /// <summary>This enum represents an internal VM register</summary>
    public enum Register
    {
        /// <summary>The value currently held in the main hand</summary>
        MAIN_HAND,
        /// <summary>The index of the next instruction to execute</summary>
        PROGRAM_COUNTER
    }
}
