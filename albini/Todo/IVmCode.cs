using Todo.Instructions;

namespace Todo
{
    /// <summary>
    /// This represents the storage of the instructions inside the VM. It
    /// allows for querying both the instruction from its index and the index
    /// from its instruction.
    /// </summary>
    public interface IVmCode
    {
        /// <summary>The number of instructions in the program</summary>
        int Size { get; }

        /// <summary>Get the instruction at the provided index</summary>
        IInstruction this[int idx] { get; }

        /// <summary>Get the index of the provided instruction</summary>
        /// <param name="instr">The instruction to look for</param>
        int InstructionIndex(IInstruction instr);
    }
}
