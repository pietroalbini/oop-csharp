using System.Collections.Generic;

namespace Todo.Tests
{
    internal class DummyVmState : IVmState
    {
        private Dictionary<Register, Value> registers;
        private Queue<Value> input;

        internal List<Value> Output { get; private set; }
        internal bool Ended { get; private set; }

        internal DummyVmState()
        {
            this.registers = new Dictionary<Register, Value>();
            this.input = new Queue<Value>();
            this.Output = new List<Value>();
            this.Ended = false;
        }

        internal void AddInputs(params Value[] values)
        {
            foreach (var value in values)
            {
                this.input.Enqueue(value);
            }
        }

        public Value GetRegister(Register register)
        {
            return this.registers.ContainsKey(register) ? this.registers[register] : Value.CreateEmpty();
        }

        public void SetRegister(Register register, Value newValue)
        {
            this.registers[register] = newValue;
        }

        public Value GetInput()
        {
            return this.input.Count > 0 ? this.input.Dequeue() : Value.CreateEmpty();
        }

        public void AddOutput(Value newValue)
        {
            this.Output.Add(newValue);
        }

        public void EndProgram()
        {
            this.Ended = true;
        }
    }
}
