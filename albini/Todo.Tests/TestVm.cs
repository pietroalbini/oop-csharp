using System.Collections.Generic;
using Todo.Instructions;
using Todo.Exceptions;
using Xunit;

namespace Todo.Tests
{
    public class TestVm
    {
        [Fact]
        public void TestEmptyVm()
        {
            var vm = new Vm(new List<IInstruction>(), new List<Value>());
            Assert.True(vm.Finished);
            Assert.Empty(vm.Output);
            Assert.Null(vm.Step());
        }

        [Fact]
        public void TestSimpleProgramStepByStep()
        {
            var vm = PrepareSimpleProgram();
            Assert.False(vm.Finished);
            Assert.Empty(vm.Output);

            Assert.Equal(Action.PICK_INPUT, vm.Step());
            Assert.False(vm.Finished);
            Assert.Empty(vm.Output);

            Assert.Equal(Action.DROP_OUTPUT, vm.Step());
            Assert.True(vm.Finished);
            Assert.Equal(new List<Value>() { Value.CreateNumber(42) }, vm.Output);

            Assert.Null(vm.Step());
        }

        [Fact]
        public void TestSimpleProgramExecute()
        {
            var vm = PrepareSimpleProgram();
            Assert.False(vm.Finished);
            Assert.Empty(vm.Output);

            vm.Execute();
            Assert.True(vm.Finished);
            Assert.Equal(new List<Value>() { Value.CreateNumber(42) }, vm.Output);
        }

        [Fact]
        public void TestLoopedProgramStepByStep()
        {
            var vm = PrepareLoopedProgram();
            Assert.False(vm.Finished);
            Assert.Empty(vm.Output);

            Assert.Equal(Action.PICK_INPUT, vm.Step());
            Assert.Equal(Action.DROP_OUTPUT, vm.Step());
            Assert.Equal(new List<Value>() { Value.CreateNumber(42) }, vm.Output);

            Assert.Equal(Action.PICK_INPUT, vm.Step());
            Assert.Equal(Action.DROP_OUTPUT, vm.Step());
            Assert.Equal(new List<Value>() { Value.CreateNumber(42), Value.CreateNumber(98) }, vm.Output);

            Assert.Null(vm.Step());
            Assert.True(vm.Finished);
        }

        [Fact]
        public void TestPositiveProgramStepByStep()
        {
            var vm = PreparePositiveProgram();
            Assert.False(vm.Finished);
            Assert.Empty(vm.Output);

            Assert.Equal(Action.PICK_INPUT, vm.Step());
            Assert.Equal(Action.DROP_OUTPUT, vm.Step());
            Assert.Equal(new List<Value>() { Value.CreateNumber(42) }, vm.Output);

            Assert.Equal(Action.PICK_INPUT, vm.Step());

            Assert.Equal(Action.DROP_MAIN_HAND, vm.Step());
            Assert.Equal(Action.PICK_INPUT, vm.Step());
            Assert.Equal(Action.DROP_OUTPUT, vm.Step());
            Assert.Equal(new List<Value>() { Value.CreateNumber(42), Value.CreateNumber(0) }, vm.Output);

            Assert.Null(vm.Step());
            Assert.True(vm.Finished);
        }

        [Fact]
        public void TestInfiniteLoopExecute()
        {
            var vm = PrepareInfiniteLoopProgram();
            Assert.Throws<InfiniteLoopException>(() => vm.Execute());
        }

        private Vm PrepareSimpleProgram()
        {
            return new Vm(
                new List<IInstruction>() { new Input(), new Output() },
                new List<Value>() { Value.CreateNumber(42) }
            );
        }

        private Vm PrepareLoopedProgram()
        {
            var jmp = new Jump();
            return new Vm(
                new List<IInstruction>() { jmp.Target, new Input(), new Output(), jmp },
                new List<Value>() { Value.CreateNumber(42), Value.CreateNumber(98) }
            );
        }

        private Vm PreparePositiveProgram()
        {
            var jmp = new Jump();
            var jmpneg = new JumpNeg();
            return new Vm(
                new List<IInstruction>() { jmp.Target, jmpneg.Target, new Input(), jmpneg, new Output(), jmp },
                new List<Value>() { Value.CreateNumber(42), Value.CreateNumber(-98), Value.CreateNumber(0) }
            );
        }

        private Vm PrepareInfiniteLoopProgram()
        {
            var jmp = new Jump();
            return new Vm(new List<IInstruction>() { jmp.Target, jmp }, new List<Value>());
        }
    }
}
