using System;
using Todo;
using Xunit;

namespace Todo.Tests
{
    public class ValueTest
    {
        [Fact]
        public void TestIsPresent() {
            Assert.False(Value.CreateEmpty().Present);
            Assert.True(Value.CreateNumber(42).Present);
            Assert.True(Value.CreateASCII('p').Present);
        }

        [Fact]
        public void TestIsASCII() {
            Assert.False(Value.CreateEmpty().IsASCII);
            Assert.False(Value.CreateNumber(42).IsASCII);
            Assert.True(Value.CreateASCII('p').IsASCII);
        }

        [Fact]
        public void TestAsNumber() {
            Assert.Throws<NullReferenceException>(() => Value.CreateEmpty().AsNumber);
            Assert.Equal(42, Value.CreateNumber(42).AsNumber);
            Assert.Equal(112, Value.CreateASCII('p').AsNumber);
        }

        [Fact]
        public void TestToString() {
            Assert.Equal("Value(empty)", Value.CreateEmpty().ToString());
            Assert.Equal("Value(42)", Value.CreateNumber(42).ToString());
            Assert.Equal("Value('p')", Value.CreateASCII('p').ToString());
        }

        [Fact]
        public void TestEquals() {
            Assert.Equal(Value.CreateEmpty(), Value.CreateEmpty());
            Assert.Equal(Value.CreateNumber(42), Value.CreateNumber(42));
            Assert.NotEqual(Value.CreateNumber(42), Value.CreateNumber(98));
            Assert.Equal(Value.CreateASCII('p'), Value.CreateASCII('p'));
            Assert.NotEqual(Value.CreateASCII('p'), Value.CreateASCII('a'));
            Assert.NotEqual(Value.CreateASCII('p'), Value.CreateNumber(112));
        }
    }
}
