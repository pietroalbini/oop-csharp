using Todo;
using Todo.Instructions;
using Xunit;

namespace Todo.Tests
{
    public class TestVmCode
    {
        [Fact]
        public void TestAdd()
        {
            var code = new VmCode();
            var instr1 = new Input();
            var instr2 = new Output();
            code.Add(instr1);
            code.Add(instr2);
            Assert.Equal(code[0].Id, instr1.Id);
            Assert.Equal(code[1].Id, instr2.Id);
            Assert.Equal(0, code.InstructionIndex(instr1));
            Assert.Equal(1, code.InstructionIndex(instr2));
            Assert.Equal(2, code.Size);
        }
    }
}
