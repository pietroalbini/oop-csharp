using Todo.Exceptions;
using Todo.Instructions;
using Xunit;

namespace Todo.Tests.Instructions
{
    public class TestOutput : BaseInstructionTest
    {
        [Fact]
        public void TestEmptyHand()
        {
            Assert.Throws<EmptyHandException>(() => AssertExecute(new Output()));
        }

        [Fact]
        public void TestValueInHand()
        {
            this.State.SetRegister(Register.MAIN_HAND, Value.CreateNumber(42));
            AssertExecute(new Output(), Action.DROP_OUTPUT);
            Assert.Equal(Value.CreateNumber(42), this.State.Output[0]);
            Assert.Single(this.State.Output);
            Assert.Equal(Value.CreateEmpty(), this.State.GetRegister(Register.MAIN_HAND));
        }
    }
}
