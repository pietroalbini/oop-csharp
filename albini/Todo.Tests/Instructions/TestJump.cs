using Todo.Instructions;
using Xunit;

namespace Todo.Tests.Instructions
{
    public class TestJump : BaseInstructionTest
    {
        [Fact]
        public void Test()
        {
            this.State.SetRegister(Register.MAIN_HAND, Value.CreateEmpty());
            AssertJump(new Jump(), true);
            this.State.SetRegister(Register.MAIN_HAND, Value.CreateNumber(42));
            AssertJump(new Jump(), true);
            this.State.SetRegister(Register.MAIN_HAND, Value.CreateNumber(-42));
            AssertJump(new Jump(), true);
            this.State.SetRegister(Register.MAIN_HAND, Value.CreateNumber(0));
            AssertJump(new Jump(), true);
        }
    }
}
