using Todo.Instructions;
using Xunit;

namespace Todo.Tests.Instructions
{
    public class TestJumpZero : BaseInstructionTest
    {
        [Fact]
        public void Test()
        {
            this.State.SetRegister(Register.MAIN_HAND, Value.CreateEmpty());
            AssertJump(new JumpZero(), false);
            this.State.SetRegister(Register.MAIN_HAND, Value.CreateNumber(42));
            AssertJump(new JumpZero(), false);
            this.State.SetRegister(Register.MAIN_HAND, Value.CreateNumber(-42));
            AssertJump(new JumpZero(), false);
            this.State.SetRegister(Register.MAIN_HAND, Value.CreateNumber(0));
            AssertJump(new JumpZero(), true);
        }
    }
}
