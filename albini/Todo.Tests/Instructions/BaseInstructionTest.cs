using Xunit;
using Todo.Instructions;
using System.Linq;
using System.Collections.Generic;

namespace Todo.Tests.Instructions
{
    public class BaseInstructionTest
    {
        internal DummyVmState State { get; private set; }

        protected BaseInstructionTest()
        {
            this.State = new DummyVmState();
        }

        protected void AssertExecute(IInstruction instr, params Action[] expectedActions)
        {
            var code = new VmCode();
            code.Add(instr);
            var executedActions = instr.Execute(this.State, code).ToList();
            Assert.Equal(expectedActions, executedActions);
        }

        protected void AssertJump(BaseJump jump, bool jumped)
        {
            var code = new VmCode();
            code.Add(jump);
            code.Add(jump.Target);
            this.State.SetRegister(Register.PROGRAM_COUNTER, Value.CreateNumber(0));
            Assert.Empty(jump.Execute(this.State, code).ToList());
            Assert.Equal(jumped ? 1 : 0, this.State.GetRegister(Register.PROGRAM_COUNTER).AsNumber);
        }
    }
}
