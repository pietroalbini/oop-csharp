using Todo.Instructions;
using Xunit;

namespace Todo.Tests.Instructions
{
    public class TestJumpNeg : BaseInstructionTest
    {
        [Fact]
        public void Test()
        {
            this.State.SetRegister(Register.MAIN_HAND, Value.CreateEmpty());
            AssertJump(new JumpNeg(), false);
            this.State.SetRegister(Register.MAIN_HAND, Value.CreateNumber(42));
            AssertJump(new JumpNeg(), false);
            this.State.SetRegister(Register.MAIN_HAND, Value.CreateNumber(-42));
            AssertJump(new JumpNeg(), true);
            this.State.SetRegister(Register.MAIN_HAND, Value.CreateNumber(0));
            AssertJump(new JumpNeg(), false);
        }
    }
}
