using Xunit;
using Todo.Instructions;

namespace Todo.Tests.Instructions
{
    public class TestInput : BaseInstructionTest
    {
        [Fact]
        public void TestEmptyHand()
        {
            this.State.AddInputs(Value.CreateNumber(42));
            AssertExecute(new Input(), Action.PICK_INPUT);
            Assert.False(this.State.Ended);
            Assert.Equal(Value.CreateNumber(42), this.State.GetRegister(Register.MAIN_HAND));
        }

        [Fact]
        public void TestFullHand()
        {
            this.State.AddInputs(Value.CreateNumber(42));
            this.State.SetRegister(Register.MAIN_HAND, Value.CreateNumber(98));
            AssertExecute(new Input(), Action.DROP_MAIN_HAND, Action.PICK_INPUT);
            Assert.False(this.State.Ended);
            Assert.Equal(Value.CreateNumber(42), this.State.GetRegister(Register.MAIN_HAND));
        }

        [Fact]
        public void TestEmptyInput()
        {
            AssertExecute(new Input());
            Assert.True(this.State.Ended);
            Assert.False(this.State.GetRegister(Register.MAIN_HAND).Present);
        }

        [Fact]
        public void TestMultipleInputs()
        {
            this.State.AddInputs(Value.CreateNumber(42), Value.CreateNumber(98));
            Assert.False(this.State.GetRegister(Register.MAIN_HAND).Present);

            AssertExecute(new Input(), Action.PICK_INPUT);
            Assert.Equal(Value.CreateNumber(42), this.State.GetRegister(Register.MAIN_HAND));
            Assert.False(this.State.Ended);

            AssertExecute(new Input(), Action.DROP_MAIN_HAND, Action.PICK_INPUT);
            Assert.Equal(Value.CreateNumber(98), this.State.GetRegister(Register.MAIN_HAND));
            Assert.False(this.State.Ended);

            AssertExecute(new Input(), Action.DROP_MAIN_HAND);
            Assert.False(this.State.GetRegister(Register.MAIN_HAND).Present);
            Assert.True(this.State.Ended);
        }
    }
}
