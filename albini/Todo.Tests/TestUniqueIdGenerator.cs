using Xunit;

namespace Todo.Tests
{
    public class TestUniqueIdGenerator
    {
        [Fact]
        public void TestGeneratedIdsAreDifferent()
        {
            Assert.NotEqual(UniqueIdGenerator.Instance.Next, UniqueIdGenerator.Instance.Next);
        }

        [Fact]
        public void TestGeneratedIdIsComparable()
        {
            var id = UniqueIdGenerator.Instance.Next;
            Assert.Equal(id, id);
        }
    }
}
