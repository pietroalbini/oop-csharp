﻿using System;
using System.Collections.Generic;
using System.Linq;
using LevelInputs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LevelInputsTest
{
    [TestClass]
    public class ModifierTest
    {
        [TestMethod]
        public void TestEnsureLastZero()
        {
            List<int?> values = new List<int?> { 0, 1, 2, 3, 4 };
            IInputModifier mod = new EnsureLastZero();
            mod.SetValues(values);
            mod.Update();
            for (int i = 0; i < values.Count - 1; i++)
            {
                if (i == (values.Count - 1))
                {
                    Assert.IsTrue(values[i] == 0);
                } else
                {
                    Assert.IsTrue(values[i] == i);
                }
            }
        }

        [TestMethod]
        public void TestEnsureZeros()
        {
            List<int?> values = new List<int?>(Enumerable.Repeat<int?>(null, 10).ToList());
            IInputModifier mod = new EnsureZeros
            {
                Count = 2
            };
            mod.SetValues(values);
            mod.Update();
            Assert.IsTrue(values.Where(val => val.HasValue).Where(val => val.Value == 0).Count() == 2);
        }

        [TestMethod]
        public void TestEnsurePositives()
        {
            List<int?> values = new List<int?>(Enumerable.Repeat<int?>(null, 10).ToList());
            IInputModifier mod = new EnsurePositives()
            {
                Count = 2
            };
            IModifierWithBound boundMod = (IModifierWithBound)mod;
            boundMod.SetBound(1);
            boundMod.SetValues(values);
            boundMod.Update();
            Assert.IsTrue(values.Where(val => val.HasValue).Where(val => val.Value > 0).Count() == 2);
        }
    }
}
