﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LevelInputs;
using System.Collections.Generic;

namespace LevelInputsTest
{
    [TestClass]
    public class GeneratorTest
    {
        [TestMethod]
        public void RandomIntegersTest()
        {
            IInputGenerator generator = new RandomIntegersGenerator(10, 10, -10);
            List<int> input = generator.GetNewInput();
            input.ForEach(i => Console.Write(i));
        }
    }
}
