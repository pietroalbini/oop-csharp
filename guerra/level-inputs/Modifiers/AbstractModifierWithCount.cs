﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LevelInputs
{
    public abstract class AbstractModifierWithCount : AbstractInputModifier
    {
        private int count;

        protected abstract override void Modify(List<int?> values);

        public int Count
        {
            protected get => this.count;
            set
            {
                if (value <= 0)
                {
                    throw new NotSupportedException("Count should be a positive number, you provided" + value);
                }
                this.count = value;
            }
        }
    }
}
