﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LevelInputs
{
    public class EnsureZeros : AbstractModifierWithCount
    {
        protected override void Modify(List<int?> values)
        {
            values.Sort(new NullIntComparator());
            for (int i = 0; i < Count; i++)
            {
                values[i] = 0;
            }
        }
    }
}
