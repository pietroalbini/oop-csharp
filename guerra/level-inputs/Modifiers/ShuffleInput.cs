﻿using System;
using System.Collections.Generic;
using Medallion;

namespace LevelInputs
{
    /// <summary>
    /// This modifier shuffles the input
    /// </summary>
    public class ShuffleInput : AbstractInputModifier
    {
        protected override void Modify(List<int?> values)
        {
            values.Shuffle();
        }
    }
}
