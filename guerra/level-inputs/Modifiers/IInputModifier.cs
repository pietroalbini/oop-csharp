﻿using System.Collections.Generic;

namespace LevelInputs
{
    /// <summary>
    /// This interface models a modifier for the input, it wraps an input and updates it when requested.
    /// </summary>
    public interface IInputModifier
    {
        /// <summary>
        /// Update the wrapped input according to this modifier's rules.
        /// </summary>
        void Update();

        /// <summary>
        /// Set the wrapped input for this modifier.
        /// </summary>
        /// <param name="values">The input to be set</param>
        void SetValues(List<int?> values);
    }
}
