﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LevelInputs
{
    public class EnsureLastZero : AbstractInputModifier
    {
        protected override void Modify(List<int?> values)
        {
            values[values.Count - 1] = 0;
        }
    }
}
