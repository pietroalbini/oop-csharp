﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LevelInputs
{
    /// <summary>
    /// This modifier ensures that a certain amount of positive values are in the input. You must set a bound
    /// for the values to be added.
    /// </summary>
    public class EnsurePositives : AbstractModifierWithCount, IModifierWithBound
    {
        private static readonly Random rand = new Random();
        private int bound;

        public void SetBound(int bound)
        {
            if (bound <= 0)
            {
                throw new NotSupportedException("Bound should be a positive number, you provided" + bound);
            }
            this.bound = bound;
        }

        protected override void Modify(List<int?> values)
        {
            values.Sort(new NullIntComparator());
            for (int i = 0; i < Count; i++)
            {
                values[i] = rand.Next(bound) + 1; 
            }
        }
    }
}
