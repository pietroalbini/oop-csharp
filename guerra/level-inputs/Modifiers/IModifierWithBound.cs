﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LevelInputs
{
    /// <summary>
    /// This interface is an extension of <see cref="IInputModifier"/>, and adds methods to deal with
    /// the fact that this modifier has a bound for maximum or minimum value to be added
    /// </summary>
    public interface IModifierWithBound : IInputModifier
    {
        /// <summary>
        /// Set the bound for elements to be added by this modifier
        /// </summary>
        /// <param name="bound">The new bound for this modifier</param>
        void SetBound(int bound);
    }
}
