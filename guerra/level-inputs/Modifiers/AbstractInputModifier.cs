﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LevelInputs
{
    public abstract class AbstractInputModifier : IInputModifier
    {
        private List<int?> input;

        public void SetValues(List<int?> values)
        {
            this.input = values ?? throw new ArgumentNullException("Values for a modifier can't be null");
        }

        public void Update()
        {
            Modify(this.input);
        }

        protected abstract void Modify(List<int?> values);
    }
}
