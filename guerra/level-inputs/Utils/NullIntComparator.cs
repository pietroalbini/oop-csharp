﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LevelInputs
{
    class NullIntComparator : IComparer<int?>
    {
        public int Compare(int? x, int? y)
        {
            if (x == null && y != null)
            {
                return -1;
            }
            if (x != null && y != null)
            {
                return 0;
            }
            return 1;
        }
    }
}
