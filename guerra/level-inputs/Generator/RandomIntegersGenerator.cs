﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LevelInputs
{
    /// <summary>
    /// This instance of IInputGenerator places random integers after making sure the modifiers have worked.
    /// </summary>
    public class RandomIntegersGenerator : IInputGenerator
    {
        private static readonly Random rand = new Random();
        private readonly List<int?> values;
        private readonly List<IInputModifier> modifiers;
        private readonly int size;
        private readonly int max;
        private readonly int min;

        public RandomIntegersGenerator(int size, int max, int min)
        {
            this.values = new List<int?>();
            this.modifiers = new List<IInputModifier>();
            this.size = size;
            this.max = max;
            this.min = min;
        }

        public List<int> GetNewInput()
        {
            this.values.Clear();
            this.values.AddRange(Enumerable.Repeat<int?>(null, size).ToList());
            this.modifiers.ForEach(mod =>
            {
                mod.SetValues(this.values);
                mod.Update();
            });
            return values.Select(val =>
            {
                return val ?? rand.Next(min, max);
            }).ToList();
        }

        public void AddModifier(IInputModifier modifier)
        {
            this.modifiers.Add(modifier);
        }
    }
}
