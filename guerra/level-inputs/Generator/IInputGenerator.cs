﻿using System.Collections.Generic;

namespace LevelInputs
{
    /// <summary>
    /// This interface models an input generator, you can add modifiers to make sure certain values
    /// are present or just generate values according to this generator's rules.
    /// </summary>
    public interface IInputGenerator
    {
        /// <summary>
        /// Generate a new input.
        /// </summary>
        /// <returns>A list of int representing an input</returns>
        List<int> GetNewInput();

        /// <summary>
        /// Add a modifier to this generator.
        /// </summary>
        /// <param name="modifier">The modifier to be added</param>
        void AddModifier(IInputModifier modifier);
    }
}
