using System;
using System.Collections.Generic;
using NUnit.Framework;
using TODO.Model.Instruction;
using TODO.Model.ProgramLogic;

namespace TODO_TEST.Model.ProgramLogic
{
    [TestFixture]
    public class TestProgram
    {
        private IProgram program;

        [SetUp]
        public void PrepareProgram()
        {
            this.program = new Program(new HashSet<Type> {typeof(Input), typeof(Output)});
        }

        [Test]
        public void TestCreation()
        {
            var exception = Assert.Throws<ArgumentException>(() => new Program(new HashSet<Type> {typeof(Input)}, new List<IInstruction> {new Output()}));
            Assert.AreEqual("Not all instructions are allowed.", exception.Message);
            exception = Assert.Throws<ArgumentException>(() => new Program(new HashSet<Type> {typeof(JumpTarget)}, new List<IInstruction> {new Jump().Target}));
            Assert.AreEqual("Outer targets are never allowed.", exception.Message);
            Assert.Throws<ArgumentException>(() => new Program(new HashSet<Type> {typeof(int)}));
            Assert.AreEqual(0, this.program.Instructions.Count);
            Assert.False(this.program.CanRedo());
            Assert.False(this.program.CanUndo());
        }

        [Test]
        public void TestHistoryNavigation()
        {
            this.program.Add(1, new Input());
            this.program.Add(2, new Output());
            this.program.Add(3, new Jump());
            this.program.Add(4, new Jump().Target);
            Assert.AreEqual(new List<IInstruction> {new Input(), new Output()}, this.program.Instructions);
            this.program.Move(1, 0);
            this.program.Remove(0);
            Assert.AreEqual(new List<IInstruction> {new Input()}, this.program.Instructions);
            this.program.Replace(0, new Output());
            this.program.Replace(0, new Jump());
            Assert.AreEqual(new List<IInstruction> {new Output()}, this.program.Instructions);
            this.program.Clear();
            Assert.AreEqual(new List<IInstruction>(), this.program.Instructions);
            this.program.Undo();
            this.program.Undo();
            this.program.Undo();
            this.program.Undo();
            Assert.AreEqual(new List<IInstruction> {new Input(), new Output()}, this.program.Instructions);
            this.program.Undo();
            this.program.Undo();
            Assert.AreEqual(0, this.program.Instructions.Count);
        }

        [Test]
        public void TestGetInstructionIndex()
        {
            program.Add(1, new Input());
            program.Add(2, new Output());
            Assert.AreEqual(1, program.GetInstructionIndex(program.Instructions[1]));
            Assert.Throws<InvalidOperationException>(() => program.GetInstructionIndex(new Input()));
        }

        [Test]
        public void TestChangeHistory()
        {
            this.program.Add(1, new Input());
            this.program.Undo();
            this.program.Add(1, new Input());
            Assert.Throws<InvalidOperationException>(() => this.program.Redo());
        }

        [Test]
        public void TestWrongUndoRedo()
        {
            Assert.Throws<InvalidOperationException>(() => this.program.Undo());
            Assert.Throws<InvalidOperationException>(() => this.program.Redo());
        }

        [Test]
        public void TestNullInsertion()
        {
            Assert.Throws<ArgumentNullException>(() => this.program.Add(1, null));
            Assert.Throws<ArgumentNullException>(() => this.program.Replace(1, null));
        }
    }
}