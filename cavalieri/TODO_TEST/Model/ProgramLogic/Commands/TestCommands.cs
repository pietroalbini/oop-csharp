using System;
using System.Collections.Generic;
using NUnit.Framework;
using TODO.Model.Instruction;
using TODO.Model.ProgramLogic.Commands;
using TODO.Utils;

namespace TODO_TEST.Model.ProgramLogic.Commands
{
    [TestFixture]
    public class TestCommands
    {
        private static readonly IList<IInstruction> BaseList = new List<IInstruction> {new Input(), new Output()};
        private static readonly IList<IInstruction> Instructions = new List<IInstruction>(BaseList);

        [SetUp]
        public void PrepareTest()
        {
            Instructions.Clear();
            Instructions.AddAll(BaseList);
        }

        private static object[] testCases =
        {
            new object[] {new AddCommand(Instructions, new Input(), 8), new List<IInstruction> {new Input(), new Output(), new Input()}},
            new object[] {new AddCommand(Instructions, new Input(), 1), new List<IInstruction> {new Input(), new Input(), new Output()}},
            new object[] {new AddCommand(Instructions, new Jump(), 1), new List<IInstruction> {new Input(), new Jump(), new Jump().Target, new Output()}},
            new object[] {new AddCommand(Instructions, new Jump(), 8), new List<IInstruction> {new Input(), new Output(), new Jump(), new Jump().Target}},
            new object[] {new RemoveCommand(Instructions, 1), new List<IInstruction> {new Input()}},
            new object[] {new RemoveCommand(Instructions, 0), new List<IInstruction> {new Output()}},
            new object[] {new MoveCommand(Instructions, 0, 1), new List<IInstruction> {new Output(), new Input()}},
            new object[] {new ClearCommand(Instructions), new List<IInstruction>()},
            new object[] {new ReplaceAllCommand(Instructions, new List<IInstruction> {new Input()}), new List<IInstruction> {new Input()}},
            new object[] {new ReplaceCommand(Instructions, 0, new Output()), new List<IInstruction> {new Output(), new Output()}}
        };

        [TestCaseSource(nameof(testCases))]
        public void TestSimpleCommands(IProgramCommand command, IList<IInstruction> expected)
        {
            TestExecuteUnexecute(command, Instructions, expected);
        }

        [Test]
        public void TestRemoveJump()
        {
            var jump = new Jump();
            var instr = new List<IInstruction> {new Input(), jump, new Output(), jump.Target};
            var command = new RemoveCommand(instr, 1);
            TestExecuteUnexecute(command, instr, BaseList);
        }

        private void TestExecuteUnexecute(IProgramCommand command, IList<IInstruction> starting, IList<IInstruction> expected)
        {
            var baseList = new List<IInstruction>(starting);
            command.Execute();
            Assert.AreEqual(expected, starting);
            command.Unexecute();
            Assert.AreEqual(baseList, starting);
        }

        [Test]
        public void TestWrongAdd()
        {
            Assert.Throws<ArgumentException>(() => new AddCommand(Instructions, new Jump().Target, 1).Execute());
        }

        [Test]
        public void TestWrongCalls()
        {
            IProgramCommand command = new ClearCommand(Instructions);
            Assert.Throws<InvalidOperationException>(() => command.Unexecute());
            command.Execute();
            Assert.Throws<InvalidOperationException>(() => command.Execute());
        }
    }
}