using NUnit.Framework;
using TODO.Utils;

namespace TODO_TEST.Utils
{
    [TestFixture]
    public class TestUniqueIdGenerator
    {
        [Test]
        public void TestGenerator()
        {
            Assert.AreNotEqual(UniqueIdGenerator.Instance.Next(), UniqueIdGenerator.Instance.Next());
        }
    }
}