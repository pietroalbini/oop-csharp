using System;
using System.Collections.Generic;
using NUnit.Framework;
using TODO.Utils;

namespace TODO_TEST.Utils
{
    [TestFixture]
    public class TestChecks
    {
        [Test]
        public void TestRequire()
        {
            Assert.Throws<ArgumentException>(() => Checks.Require<ArgumentException>(false));
            Assert.DoesNotThrow(() => Checks.Require<ArgumentException>(true));
        }

        [Test]
        public void TestRequireOnIterable()
        {
            var enumerable = new List<int> {1, 2, 3};
            Assert.Throws<ArgumentException>(() => Checks.RequireOnEnumerable<ArgumentException, int>(i => i < 3, enumerable));
            Assert.DoesNotThrow(() => Checks.RequireOnEnumerable<ArgumentException, int>(i => i <= 3, enumerable));
        }
    }
}