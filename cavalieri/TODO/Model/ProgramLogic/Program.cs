using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using TODO.Model.Instruction;
using TODO.Model.ProgramLogic.Commands;
using TODO.Utils;

namespace TODO.Model.ProgramLogic
{
    public class Program : IProgram
    {
        private const int MAX_HISTORY_LAYERS = 20;

        private readonly ISet<Type> allowedInstructions;
        private readonly IList<IProgramCommand> commandHistory;
        private readonly IList<IInstruction> instructions;
        private int historyPosition;

        public Program(ISet<Type> allowedInstructions)
        {
            this.historyPosition = 0;
            this.instructions = new List<IInstruction>();
            this.commandHistory = new List<IProgramCommand>();
            Checks.Require<ArgumentException>(!allowedInstructions.Contains(typeof(JumpTarget)), "Outer targets are never allowed.");
            Checks.RequireOnEnumerable<ArgumentException, Type>(t => typeof(IInstruction).IsAssignableFrom(t), allowedInstructions);
            this.allowedInstructions = new HashSet<Type>(allowedInstructions);
        }

        public Program(ISet<Type> allowedInstructions, IList<IInstruction> instructions) : this(allowedInstructions)
        {
            Checks.RequireOnEnumerable<ArgumentException, IInstruction>(IsInstructionAllowed, instructions, "Not all instructions are allowed.");
            this.instructions.AddAll(instructions);
        }

        public IImmutableList<IInstruction> Instructions => ImmutableList.CreateRange(instructions);

        public int GetInstructionIndex(IInstruction instruction)
        {
            var index = this.Instructions.Select(instr => instr.Id)
                                         .ToList()
                                         .IndexOf(instruction.Id);
            Checks.Require<InvalidOperationException>(index != -1);
            return index;
        }

        private bool IsInstructionAllowed(IInstruction instruction)
        {
            return this.allowedInstructions.Contains(instruction.GetType());
        }

        public void Move(int from, int to)
        {
            if (from != to)
            {
                AddNewCommand(new MoveCommand(this.instructions, from, to));
            }
        }

        public void Replace(int index, IInstruction instruction)
        {
            if (IsInstructionAllowed(instruction ?? throw new ArgumentNullException()))
            {
                AddNewCommand(new ReplaceCommand(this.instructions, index, instruction));
            }
        }

        public void Add(int to, IInstruction instruction)
        {
            if (IsInstructionAllowed(instruction ?? throw new ArgumentNullException()))
            {
                AddNewCommand(new AddCommand(this.instructions, instruction, to));
            }
        }

        public void Remove(int from)
        {
            AddNewCommand(new RemoveCommand(this.instructions, from));
        }

        public void Clear()
        {
            if (this.instructions.Count != 0)
            {
                AddNewCommand(new ClearCommand(this.instructions));
            }
        }

        private void AddNewCommand(IProgramCommand command)
        {
            if (CanRedo())
            {
                this.commandHistory.ClearSection(this.historyPosition, this.commandHistory.Count);
            }

            if (this.commandHistory.Count == MAX_HISTORY_LAYERS)
            {
                this.commandHistory.RemoveAt(0);
                this.historyPosition--;
            }

            command.Execute();
            this.commandHistory.Add(command);
            this.historyPosition++;
        }

        public bool CanUndo()
        {
            return this.historyPosition > 0;
        }

        public void Undo()
        {
            Checks.Require<InvalidOperationException>(CanUndo());
            this.historyPosition--;
            this.commandHistory[this.historyPosition].Unexecute();
        }

        public bool CanRedo()
        {
            return this.historyPosition < this.commandHistory.Count;
        }

        public void Redo()
        {
            Checks.Require<InvalidOperationException>(CanRedo());
            this.commandHistory[this.historyPosition].Execute();
            this.historyPosition++;
        }

        public void Copy()
        {
            throw new NotImplementedException();
        }

        public void Paste()
        {
            throw new NotImplementedException();
        }
    }
}