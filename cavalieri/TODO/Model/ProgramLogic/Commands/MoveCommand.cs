using System.Collections.Generic;
using TODO.Model.Instruction;

namespace TODO.Model.ProgramLogic.Commands

{
    public class MoveCommand : AbstractCommand
    {
        private readonly int from;
        private readonly int to;

        public MoveCommand(IList<IInstruction> instructions, int from, int to) : base(instructions)
        {
            this.from = from;
            this.to = to;
        }

        protected override void OnExecute()
        {
            Move(this.from, this.to);
        }

        protected override void OnUnexecute()
        {
            Move(this.to, this.from);
        }

        private void Move(int from, int to)
        {
            if (from != to)
            {
                var temp = this.Instructions[from];
                this.Instructions[from] = this.Instructions[to];
                this.Instructions[to] = temp;
            }
        }
    }
}