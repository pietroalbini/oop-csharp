using System.Collections.Generic;
using System.Linq;
using TODO.Model.Instruction;
using TODO.Utils;

namespace TODO.Model.ProgramLogic.Commands
{
    public class RemoveCommand : AbstractCommand
    {
        private readonly int from;
        private readonly IInstruction removed;
        private int? connectedIndex;
        private IInstruction connected;


        public RemoveCommand(IList<IInstruction> instructions, int from) : base(instructions)
        {
            this.from = from;
            this.removed = instructions[from];
            this.connectedIndex = null;
            this.connected = null;
        }

        protected override void OnExecute()
        {
            this.Instructions.RemoveAt(this.from);
            SetConnectedInstruction();
            if (this.connectedIndex.HasValue)
            {
                this.Instructions.RemoveAt(this.connectedIndex.Value);
            }
        }

        protected override void OnUnexecute()
        {
            if (this.connectedIndex.HasValue)
            {
                this.Instructions.Insert(this.connectedIndex.Value, this.connected);
            }
            this.Instructions.Insert(this.from, this.removed);
        }

        private void SetConnectedInstruction()
        {
            if (this.removed is Jump instruction)
            {
                this.connected = instruction.Target;
                this.connectedIndex = IndexOf(this.connected.Id);
            }
            else if (this.removed is JumpTarget target)
            {
                this.connected = this.Instructions[IndexOf(target.SourceId)];
                this.connectedIndex = IndexOf(this.connected.Id);
            }
        }

        private int IndexOf(IUniqueId id)
        {
            return this.Instructions.Select(instr => instr.Id).ToList().IndexOf(id);
        }
    }
}