using System;
using System.Collections.Generic;
using TODO.Model.Instruction;

namespace TODO.Model.ProgramLogic.Commands
{
    public class ReplaceCommand : AbstractCommand
    {
        private readonly int from;
        private readonly IInstruction instruction;
        private readonly IInstruction replaced;

        public ReplaceCommand(IList<IInstruction> instructions, int from, IInstruction instruction) : base(instructions)
        {
            this.from = from;
            this.instruction = instruction ?? throw new ArgumentNullException();
            this.replaced = instructions[from];
        }

        protected override void OnExecute()
        {
            this.Instructions[this.from] = this.instruction;
        }

        protected override void OnUnexecute()
        {
            this.Instructions[this.from] = this.replaced;
        }
    }
}