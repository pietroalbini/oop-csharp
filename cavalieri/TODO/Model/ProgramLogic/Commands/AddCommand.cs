using System;
using System.Collections.Generic;
using TODO.Model.Instruction;
using TODO.Utils;

namespace TODO.Model.ProgramLogic.Commands
{
    public class AddCommand : AbstractCommand
    {
        private readonly IInstruction toAdd;
        private readonly int to;

        public AddCommand(IList<IInstruction> instructions, IInstruction toAdd, int to) : base(instructions)
        {
            Checks.Require<ArgumentException>(!(toAdd is JumpTarget));
            this.toAdd = toAdd ?? throw new ArgumentNullException();
            this.to = to;
        }

        protected override void OnExecute()
        {
            AddToList(this.toAdd, this.to);
            if (this.toAdd is Jump instruction)
            {
                AddToList(instruction.Target, this.to + 1);
            }
        }

        protected override void OnUnexecute()
        {
            if (this.toAdd is Jump)
            {
                RemoveFromList(this.to + 1);
            }
            RemoveFromList(this.to);
        }

        private void AddToList(IInstruction instruction, int to)
        {
            this.Instructions.Insert(to > this.Instructions.Count ? this.Instructions.Count : to, instruction);
        }

        private void RemoveFromList(int from)
        {
            this.Instructions.RemoveAt(from >= this.Instructions.Count ? this.Instructions.Count - 1 : from);
        }
    }
}