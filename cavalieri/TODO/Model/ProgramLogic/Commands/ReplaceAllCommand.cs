using System;
using System.Collections.Generic;
using TODO.Model.Instruction;
using TODO.Utils;

namespace TODO.Model.ProgramLogic.Commands
{
    public class ReplaceAllCommand : AbstractCommand
    {
        private readonly IList<IInstruction> newInstructions;
        private readonly IList<IInstruction> oldInstructions;

        public ReplaceAllCommand(IList<IInstruction> instructions, IEnumerable<IInstruction> newInstructions) : base(instructions)
        {
            this.newInstructions = new List<IInstruction>(newInstructions ?? throw new ArgumentNullException());
            this.oldInstructions = new List<IInstruction>(instructions);
        }

        protected override void OnExecute()
        {
            this.Instructions.Clear();
            this.Instructions.AddAll(this.newInstructions);
        }

        protected override void OnUnexecute()
        {
            this.Instructions.Clear();
            this.Instructions.AddAll(this.oldInstructions);
        }
    }
}