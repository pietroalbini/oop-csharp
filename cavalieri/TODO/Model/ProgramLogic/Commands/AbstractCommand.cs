using System;
using System.Collections.Generic;
using TODO.Model.Instruction;
using TODO.Utils;

namespace TODO.Model.ProgramLogic.Commands
{
    public abstract class AbstractCommand : IProgramCommand
    {
        protected readonly IList<IInstruction> Instructions;
        private bool executed;

        protected AbstractCommand(IList<IInstruction> instructions)
        {
            this.Instructions = instructions ?? throw new ArgumentNullException();
            this.executed = false;
        }

        public void Execute()
        {
            Checks.Require<InvalidOperationException>(!this.executed);
            this.executed = true;
            OnExecute();
        }

        public void Unexecute()
        {
            Checks.Require<InvalidOperationException>(this.executed);
            this.executed = false;
            OnUnexecute();
        }

        protected abstract void OnExecute();

        protected abstract void OnUnexecute();
    }
}