using System;

namespace TODO.Model.ProgramLogic.Commands

{
    public interface IProgramCommand
    {
        /// <summary>The Command performs its operation on the receiving program.</summary>
        /// <exception cref = "InvalidOperationException"> if it is called twice in a row </exception>
        void Execute();

        /// <summary>The Command un-executes its operation on the receiving program.</summary>
        /// <exception cref = "InvalidOperationException"> if it is not called immediately after execute </exception>
        void Unexecute();
    }
}