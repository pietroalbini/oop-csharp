using System.Collections.Generic;
using TODO.Model.Instruction;
using TODO.Utils;

namespace TODO.Model.ProgramLogic.Commands
{
    public class ClearCommand : AbstractCommand
    {
        private readonly IList<IInstruction> removed;

        public ClearCommand(IList<IInstruction> instructions) : base(instructions)
        {
            this.removed = new List<IInstruction>(instructions);
        }

        protected override void OnExecute()
        {
            this.Instructions.Clear();
        }

        protected override void OnUnexecute()
        {
            this.Instructions.AddAll(this.removed);
        }
    }
}