using System.Collections.Immutable;
using TODO.Model.Instruction;

namespace TODO.Model.ProgramLogic
{
    public interface IProgram
    {
        /// <returns> An immutable view of the instructions of the program </returns>
        IImmutableList<IInstruction> Instructions { get; }

        /// <param name="instruction"> the <see cref="IInstruction"/> whose index the user wants to know </param>
        /// <returns> the index of the instruction required </returns>
        int GetInstructionIndex(IInstruction instruction);

        /// <param name="from"> the index where the instruction at index to will be placed </param>
        /// <param name="to"> the index where the instruction at index from will be placed </param>
        void Move(int from, int to);

        /// <param name="index"> the index of the instruction to be replaced </param>
        /// <param name="instruction"> the <see cref="IInstruction"/> that will replace the previous one </param>
        void Replace(int index, IInstruction instruction);

        /// <param name="to"> the index where the instruction will be added (shifting to the right all other instructions) </param>
        /// <param name="instruction"> the <see cref="IInstruction"/> to be added </param>
        void Add(int to, IInstruction instruction);

        /// <param name="from"> the index where the instruction will be removed </param>
        void Remove(int from);

        /// <summary> Removes all instructions from the program. </summary>
        void Clear();

        /// <returns> true if the last action can be undone </returns>
        bool CanUndo();

        /// <summary> Undoes the last performed change to the program. </summary>
        void Undo();

        /// <returns> true if there is a change that can be redone </returns>
        bool CanRedo();

        /// <summary> Redoes the last un-executed change to the program. </summary>
        void Redo();

        void Copy();

        void Paste();
    }
}