namespace TODO.Model.Instruction
{
    public class Output : BaseInstruction
    {
        public override bool Equals(object obj)
        {
            return obj is Output;
        }

        public override string ToString()
        {
            return "Output";
        }
    }
}