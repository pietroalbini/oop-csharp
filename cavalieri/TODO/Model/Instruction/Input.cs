namespace TODO.Model.Instruction
{
    public class Input : BaseInstruction
    {
        public override bool Equals(object obj)
        {
            return obj is Input;
        }

        public override string ToString()
        {
            return "Input";
        }
    }
}