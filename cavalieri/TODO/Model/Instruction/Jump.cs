namespace TODO.Model.Instruction
{
    public class Jump : BaseInstruction
    {
        public JumpTarget Target { get; }

        public Jump()
        {
            Target = new JumpTarget(Id);
        }

        public override bool Equals(object obj)
        {
            return obj is Jump;
        }
    }
}