using TODO.Utils;

namespace TODO.Model.Instruction
{
    public class BaseInstruction : IInstruction
    {
        public IUniqueId Id { get; }

        protected BaseInstruction()
        {
            this.Id = UniqueIdGenerator.Instance.Next();
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }
    }
}