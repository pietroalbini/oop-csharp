using TODO.Utils;

namespace TODO.Model.Instruction
{
    public class JumpTarget : BaseInstruction
    {
        public IUniqueId SourceId { get; }

        public JumpTarget(IUniqueId sourceId)
        {
            SourceId = sourceId;
        }

        public override bool Equals(object obj)
        {
            return obj is JumpTarget;
        }
    }
}