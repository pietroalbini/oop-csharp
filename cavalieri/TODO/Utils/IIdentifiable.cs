namespace TODO.Utils
{
    public interface IIdentifiable
    {
        IUniqueId Id { get; }
    }
}