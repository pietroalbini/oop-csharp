using System;
using System.Collections.Generic;
using System.Linq;

namespace TODO.Utils
{
    public static class Checks
    {
        public static void Require<TE>(bool condition) where TE : Exception, new()
        {
            if (!condition)
            {
                throw new TE();
            }
        }

        public static void Require<TE>(bool condition, string message) where TE : Exception
        {
            if (!condition)
            {
                throw (TE) Activator.CreateInstance(typeof(TE), message);
            }
        }

        public static void RequireOnEnumerable<TE, T>(Func<T, bool> predicate, IEnumerable<T> enumerable) where TE : Exception, new()
        {
            Require<TE>(enumerable.All(predicate.Invoke));
        }

        public static void RequireOnEnumerable<TE, T>(Func<T, bool> predicate, IEnumerable<T> enumerable, string message) where TE : Exception
        {
            Require<TE>(enumerable.All(predicate.Invoke), message);
        }
    }
}