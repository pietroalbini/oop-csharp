namespace TODO.Utils
{
    public class UniqueIdGenerator
    {
        public static UniqueIdGenerator Instance { get; } = new UniqueIdGenerator();
        private int counter;

        private UniqueIdGenerator()
        {
            counter = 0;
        }

        public IUniqueId Next()
        {
            return new UniqueId(counter++);
        }

        private class UniqueId : IUniqueId
        {
            private readonly int id;

            public UniqueId(int id)
            {
                this.id = id;
            }

            public override bool Equals(object obj)
            {
                return obj is UniqueId uniqueId && uniqueId.id == id;
            }

            public override int GetHashCode()
            {
                return id.GetHashCode();
            }
        }
    }
}