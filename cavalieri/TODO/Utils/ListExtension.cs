using System;
using System.Collections.Generic;

namespace TODO.Utils
{
    public static class ListExtension
    {
        public static void AddAll<T>(this IList<T> list, IEnumerable<T> items)
        {
            if (list is List<T> list1)
            {
                list1.AddRange(items);
            }
            else
            {
                foreach (var item in items)
                {
                    list.Add(item);
                }
            }
        }

        public static void ClearSection<T>(this IList<T> list, int fromInclusive, int toExclusive)
        {
            Checks.Require<ArgumentException>(fromInclusive < toExclusive);
            for (int i = toExclusive - 1; i >= fromInclusive; i--)
            {
                list.RemoveAt(i);
            }
        }
    }
}