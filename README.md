# Task C# per il progetto OOP

Questo repository contiene i task C# di Albini Pietro, Cavalieri Giacomo, Di
Domenico Nicolò e Guerra Michele.

## Note per il task di Di Domenico Nicolò

Il task fa uso di funzionalità disponibili solo su C# 8, quindi è richiesto
l'uso di Visual Studio 2019 per poter aprire e compilare la soluzione.

## Note per il task di Albini Pietro

Il mio task reimplementa la VM ed un sottoinsieme di istruzioni in C#, ma con
un approccio differente rispetto al progetto in Java.

Uno dei requisiti della VM è mostrare uno stato coerente alla view durante le
animazioni, ma un'istruzione può richiedere diverse animazioni separate. Per
esempio, se viene eseguita l'istruzione `input` con un valore attaccato alla
navicella, prima deve essere animata la rimozione del valore corrente e
successivamente il prendere un nuovo elemento di input. Mentre la view sta
animando la rimozione però la VM non può aver già eseguito il codice che prende
il nuovo elemento, perché fornirebbe uno stato inconsistente rispetto a ciò che
la view si aspetta.

Serve quindi un modo di eseguire solo parte dell'istruzione prima di una
animazione, eseguendo il resto del codice solo dopo il completamento
dell'animazione precedente. La soluzione più elegante a questo problema è
utilizzare funzioni riesumabili (coroutine/generatori), ma Java non le
implementa. Nel progetto ho quindi dovuto realizzare un design alternativo, le
microistruzioni, spiegato più nel dettaglio nella relazione.

Visto che il task C# doveva mettere in evidenza le caratteristiche di C# e
dato che C# supporta le funzioni riesumabili grazie a `yield return`, ho
preferito non implementare le microistruzioni utilizzando invece le funzioni
riesumabili direttamente nelle istruzioni. L'architettura implementata è quindi
diversa rispetto a Java.
