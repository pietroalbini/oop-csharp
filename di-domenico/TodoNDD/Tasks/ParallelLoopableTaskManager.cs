﻿using System.Collections.Generic;

namespace TodoNDD.Tasks
{
    public sealed class ParallelLoopableTaskManager : BaseLoopableTaskManager<List<ILoopableTask>>
    {
        public override void Tick(float deltaTime)
        {
            // Tick every task, then do the cleanup phase separately
            foreach (var task in Container)
            {
                task.OnTick(deltaTime);
                if (task.IsDone)
                {
                    task.OnDestroy();
                }
            }
            Container.RemoveAll(t => t.IsDone);
        }
    }
}
