﻿namespace TodoNDD.Tasks
{
    public interface ILoopableTaskManager
    {
        int TasksCount {
            get;
        }
        void Add(ILoopableTask task);
        void Remove(ILoopableTask task);
        void RemoveAll();
        void Tick(float deltaTime);
    }
}
