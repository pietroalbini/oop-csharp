﻿using System.Collections.Generic;

namespace TodoNDD.Tasks
{
    public abstract class BaseLoopableTaskManager<C> : ILoopableTaskManager where C : ICollection<ILoopableTask>, new()
    {
        public int TasksCount => Container.Count;
        protected C Container {
            get;
        }

        public BaseLoopableTaskManager()
        {
            Container = new C();
        }

        public void Add(ILoopableTask task)
        {
            Container.Add(task);
            task.OnInit();
        }

        public void Remove(ILoopableTask task)
        {
            Container.Remove(task);
            task.OnDestroy();
        }

        public void RemoveAll()
        {
            foreach (var task in Container)
            {
                task.OnDestroy();
            }
            Container.Clear();
        }

        public abstract void Tick(float deltaTime);
    }
}
