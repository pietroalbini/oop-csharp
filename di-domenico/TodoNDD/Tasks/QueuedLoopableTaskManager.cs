﻿using System.Collections.Generic;

namespace TodoNDD.Tasks
{
    public sealed class QueuedLoopableTaskManager :
        BaseLoopableTaskManager<LinkedList<ILoopableTask>>,
        ISequentialLoopableTaskManager
    {
        public void AbortCurrentTask()
        {
            if (Container.Count > 0)
            {
                Container.First.Value.OnDestroy();
                Container.RemoveFirst();
            }
        }

        public void SwitchToTask(ILoopableTask task)
        {
            if (Container.Count > 0)
            {
                Container.First.Value.OnDestroy();
                Container.RemoveFirst();
            }
            Container.AddFirst(task);
            task.OnInit();
        }

        public override void Tick(float deltaTime)
        {
            // Tick only the first task
            var toTick = Container.First?.Value;
            toTick?.OnTick(deltaTime);
            // If the task is present and is done, remove it
            if (toTick?.IsDone ?? false)
            {
                toTick.OnDestroy();
                Container.RemoveFirst();
            }
        }
    }
}
