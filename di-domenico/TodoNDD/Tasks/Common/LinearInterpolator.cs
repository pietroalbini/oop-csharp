﻿using System;

namespace TodoNDD.Tasks.Common
{
    public sealed class LinearInterpolator : IInterpolator<float>
    {
        public float Interpolate(float from, float to, float alpha) => from * (1 - Math.Clamp(alpha, 0f, 1f)) + to * Math.Clamp(alpha, 0f, 1f);
    }
}
