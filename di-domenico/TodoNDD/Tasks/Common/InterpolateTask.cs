﻿using System;

namespace TodoNDD.Tasks.Common
{
    public class InterpolateTask<T> : ILoopableTask where T : IEquatable<T>
    {
        private readonly IInterpolator<T> interpolator;
        private readonly Action<T> setter;
        private readonly T from;
        private readonly T to;
        private readonly float duration;
        private float alpha;
        private float currentTime;

        public bool IsDone => alpha >= 1;

        public InterpolateTask(IInterpolator<T> interpolator, T from, T to, float duration, Action<T> setter)
        {
            this.interpolator = interpolator;
            this.setter = setter;
            this.from = from;
            this.to = to;
            this.duration = duration;
        }
        public void OnInit()
        {
            alpha = 0;
            currentTime = 0f;
        }

        public void OnDestroy()
        {
            // Do nothing
        }

        public void OnTick(float deltaTime)
        {
            currentTime += deltaTime;
            alpha = Math.Min(1f, currentTime / duration);
            setter(interpolator.Interpolate(from, to, alpha));
        }
    }
}
