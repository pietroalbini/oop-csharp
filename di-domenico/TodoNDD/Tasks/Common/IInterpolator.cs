﻿using System;

namespace TodoNDD.Tasks.Common
{
    public interface IInterpolator<T> where T : IEquatable<T>
    {
        T Interpolate(T from, T to, float alpha);
    }
}
