﻿using System.Numerics;

namespace TodoNDD.Tasks.Common
{
    public sealed class Vector2Interpolator : IInterpolator<Vector2>
    {
        private IInterpolator<float> xInterpolator;
        private IInterpolator<float> yInterpolator;

        public Vector2Interpolator(IInterpolator<float> xInterpolator, IInterpolator<float> yInterpolator)
        {
            this.xInterpolator = xInterpolator;
            this.yInterpolator = yInterpolator;
        }

        public Vector2 Interpolate(Vector2 from, Vector2 to, float alpha)
        {
            return new Vector2(xInterpolator.Interpolate(from.X, to.X, alpha), yInterpolator.Interpolate(from.Y, to.Y, alpha));
        }
    }
}
