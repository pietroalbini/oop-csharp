﻿namespace TodoNDD.Tasks
{
    public interface ISequentialLoopableTaskManager : ILoopableTaskManager
    {
        void SwitchToTask(ILoopableTask task);
        void AbortCurrentTask();
    }
}
