﻿namespace TodoNDD.Tasks
{
    public interface ILoopableTaskAcceptor<M> where M : ILoopableTaskManager
    {
        M LoopableTaskManager {
            get;
        }
    }
}
