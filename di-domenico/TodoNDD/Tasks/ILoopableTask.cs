﻿namespace TodoNDD.Tasks
{
    public interface ILoopableTask
    {
        void OnInit();
        void OnTick(float deltaTime);
        void OnDestroy();
        bool IsDone {
            get;
        }
    }
}
