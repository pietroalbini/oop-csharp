﻿namespace TodoNDD.Entities
{
    public interface IUpdateable
    {
        void Update(float deltaTime);
    }
}
