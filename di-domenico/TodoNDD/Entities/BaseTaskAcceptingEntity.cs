﻿using TodoNDD.Tasks;

namespace TodoNDD.Entities
{
    public abstract class BaseTaskAcceptingEntity<M> : BaseEntity, ITaskAcceptingEntity<M>
        where M : ILoopableTaskManager
    {
        public abstract M LoopableTaskManager
        {
            get;
        }

        public sealed override void Update(float deltaTime)
        {
            if (LoopableTaskManager.TasksCount == 0)
            {
                FallbackUpdate(deltaTime);
            }
            else
            {
                LoopableTaskManager.Tick(deltaTime);
            }
        }

        public virtual void FallbackUpdate(float deltaTime)
        {
            // Do nothing
        }
    }
}
