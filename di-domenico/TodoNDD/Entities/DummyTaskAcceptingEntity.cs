﻿using TodoNDD.Tasks;

namespace TodoNDD.Entities
{
    public sealed class DummyTaskAcceptingEntity : BaseTaskAcceptingEntity<ISequentialLoopableTaskManager>
    {
        public override ISequentialLoopableTaskManager LoopableTaskManager => manager;
        private ISequentialLoopableTaskManager manager;

        internal DummyTaskAcceptingEntity() => this.manager = new QueuedLoopableTaskManager();
    }
}
