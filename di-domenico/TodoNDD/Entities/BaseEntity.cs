﻿using System.Numerics;

namespace TodoNDD.Entities
{
    public abstract class BaseEntity : IEntity
    {
        public Vector2 Position
        {
            get;
            set;
        }
        public Vector2 RelativePosition
        {
            get => Position - (Parent?.Position ?? Vector2.Zero);
            set => Position = (Parent?.Position ?? Vector2.Zero) + value;
        }
        public float Rotation
        {
            get;
            set;
        }
        public float RelativeRotation
        {
            get => Rotation - (Parent?.Rotation ?? 0f);
            set => Rotation = (Parent?.Rotation ?? 0f) + value;
        }
        public Vector2 Scale
        {
            get;
            set;
        }
        public Vector2 RelativeScale
        {
            get
            {
                if (Parent != null)
                {
                    return Parent.Scale != Vector2.Zero ?
                        new Vector2(Scale.X / Parent.Scale.X, Scale.Y / Parent.Scale.Y) :
                        Vector2.Zero;
                }
                else
                {
                    return Scale;
                }
            }
            set
            {
                Scale = new Vector2((Parent?.Scale.X ?? 1f) * value.X, (Parent?.Scale.Y ?? 1f) * value.Y);
            }
        }
        public IEntity? Parent
        {
            get;
            set;
        }

        internal BaseEntity()
        {
            Position = Vector2.Zero;
            Rotation = 0f;
            Scale = Vector2.One;
        }

        public void RemoveParent()
        {
            Parent = null;
        }

        public abstract void Update(float deltaTime);
    }
}
