﻿using System.Numerics;

namespace TodoNDD.Entities
{
    public interface IEntity : IUpdateable
    {
        Vector2 Position
        {
            get;
            set;
        }
        Vector2 RelativePosition
        {
            get;
            set;
        }
        float Rotation
        {
            get;
            set;
        }
        float RelativeRotation
        {
            get;
            set;
        }
        Vector2 Scale
        {
            get;
            set;
        }
        Vector2 RelativeScale
        {
            get;
            set;
        }
        IEntity? Parent
        {
            get;
            set;
        }
        void RemoveParent();
    }
}
