﻿namespace TodoNDD.Entities.Builders
{
    public sealed class DummyEntityBuilder : BaseEntityBuilder<DummyEntityBuilder, DummyEntity>
    {
        public override DummyEntity CallConstructor() => new DummyEntity();
    }
}
