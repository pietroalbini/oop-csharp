﻿using System.Numerics;

namespace TodoNDD.Entities.Builders
{
    public abstract class BaseEntityBuilder<S, E> : IEntityBuilder<S, E>
        where S : BaseEntityBuilder<S, E>
        where E : IEntity
    {
        private Vector2 position = Vector2.Zero;
        private bool isPositionRelative = false;
        private float rotation = 0f;
        private bool isRotationRelative = false;
        private Vector2 scale = new Vector2(1f, 1f);
        private bool isScaleRelative = false;
        private IEntity? parent = null;

        public E Build()
        {
            var built = CallConstructor();
            built.Parent = parent;
            if (isPositionRelative)
            {
                built.RelativePosition = position;
            }
            else
            {
                built.Position = position;
            }
            if (isRotationRelative)
            {
                built.RelativeRotation = rotation;
            }
            else
            {
                built.Rotation = rotation;
            }
            if (isScaleRelative)
            {
                built.RelativeScale = scale;
            }
            else
            {
                built.Scale = scale;
            }
            return AdditionalBuild(built);
        }

        public S Parent(IEntity parent)
        {
            this.parent = parent;
            return (S) this;
        }

        public S Position(Vector2 position)
        {
            this.position = position;
            this.isPositionRelative = false;
            return (S) this;
        }

        public S RelativePosition(Vector2 relativePosition)
        {
            this.position = relativePosition;
            this.isPositionRelative = true;
            return (S) this;
        }

        public S RelativeRotation(float relativeRadians)
        {
            this.rotation = relativeRadians;
            this.isRotationRelative = true;
            return (S) this;
        }

        public S RelativeScale(Vector2 relativeScale)
        {
            this.scale = relativeScale;
            this.isScaleRelative = true;
            return (S) this;
        }

        public S Rotation(float radians)
        {
            this.rotation = radians;
            this.isRotationRelative = false;
            return (S) this;
        }

        public S Scale(Vector2 scale)
        {
            this.scale = scale;
            this.isScaleRelative = false;
            return (S) this;
        }

        public abstract E CallConstructor();

        public virtual E AdditionalBuild(E built)
        {
            // Do nothing
            return built;
        }
    }
}
