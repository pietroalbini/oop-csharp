﻿namespace TodoNDD.Entities.Builders
{
    public sealed class DummyTaskAcceptingEntityBuilder
        : BaseEntityBuilder<DummyTaskAcceptingEntityBuilder, DummyTaskAcceptingEntity>
    {
        public override DummyTaskAcceptingEntity CallConstructor() => new DummyTaskAcceptingEntity();
    }
}
