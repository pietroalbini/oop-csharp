﻿using System.Numerics;

namespace TodoNDD.Entities.Builders
{
    public interface IEntityBuilder<S, E>
        where S : IEntityBuilder<S, E>
        where E : IEntity
    {
        S Position(Vector2 position);
        S Rotation(float radians);
        S Scale(Vector2 scale);
        S RelativePosition(Vector2 relativePosition);
        S RelativeRotation(float relativeRadians);
        S RelativeScale(Vector2 relativeScale);
        S Parent(IEntity parent);
        E Build();
    }
}
