﻿using TodoNDD.Tasks;

namespace TodoNDD.Entities
{
    public interface ITaskAcceptingEntity<M> : IEntity, ILoopableTaskAcceptor<M> where M : ILoopableTaskManager
    {
        void FallbackUpdate(float deltaTime);
    }
}
