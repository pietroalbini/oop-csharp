﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using TodoNDD.Tasks;

namespace TodoNDDTests
{
    [TestClass]
    public sealed class ParallelLoopableTaskManagerTest : BaseLoopableTaskManagerTest<ParallelLoopableTaskManager>
    {
        private const int Tasks = 2;
        private const int TicksPerTask = 4;

        public ParallelLoopableTaskManagerTest() : base(Tasks, TicksPerTask)
        {
        }

        protected override string GetExpectedMultipleTasksOutput(int tasksCount, int loops)
        {
            var result = new StringBuilder();
            // Initialize all
            for (int i = 0; i < tasksCount; i++)
            {
                AppendWithID(result, i, "I");
            }
            // Tick all
            for (int i = 0; i < loops; i++)
            {
                for (int j = 0; j < tasksCount; j++)
                {
                    AppendWithID(result, j, "T");
                    if (i == loops - 1)
                    {
                        // Destroy if it's the last task
                        AppendWithID(result, j, "D");
                    }
                }
            }
            return result.ToString();
        }
    }
}
