﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Numerics;
using System.Linq;
using TodoNDD.Entities;
using TodoNDD.Entities.Builders;
using TodoNDD.Tasks.Common;

namespace TodoNDDTests
{
    [TestClass]
    public sealed class DummyTaskAcceptingEntityTest :
        BaseEntityTest<DummyTaskAcceptingEntityBuilder, DummyTaskAcceptingEntity>
    {
        private const float Epsilon = 0.001f;

        [TestMethod]
        public void TestTaskManager()
        {
            var positions = new List<Vector2>();
            var expectedPositions = Enumerable.Range(0, 10)
                                              .Select(i => i / 10f + 0.1f)
                                              .Select(i => new Vector2(i, i))
                                              .ToList();
            // Make the entity
            var entity = Builder.Build();
            entity.LoopableTaskManager.Add(
                new InterpolateTask<Vector2>(
                    new Vector2Interpolator(
                        new LinearInterpolator(),
                        new LinearInterpolator()
                    ),
                    Vector2.Zero,
                    Vector2.One,
                    1f,
                    p => entity.Position = p
                )
            );
            // Simulate 0.1s-long ticks
            while (entity.LoopableTaskManager.TasksCount > 0)
            {
                entity.Update(0.1f);
                positions.Add(entity.Position);
            }
            Assert.AreEqual(expectedPositions.Count, positions.Count);
            for (int i = 0; i < expectedPositions.Count; i++)
            {
                Assert.AreEqual(expectedPositions[i].X, positions[i].X, Epsilon);
                Assert.AreEqual(expectedPositions[i].Y, positions[i].Y, Epsilon);
            }
        }
    }
}
