﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Numerics;
using TodoNDD.Entities;
using TodoNDD.Entities.Builders;

namespace TodoNDDTests
{
    [TestClass]
    public abstract class BaseEntityTest<B, E>
        where B : IEntityBuilder<B, E>, new()
        where E : IEntity
    {
        private static readonly Vector2 Position = new Vector2(42, 42);
        private const float Rotation = (float) Math.PI / 2;
        private static readonly Vector2 Scale = new Vector2(2, 2);

        protected IEntityBuilder<B, E> Builder => new B();

        [TestMethod]
        public void TestAbsolute()
        {
            // Make the entity with no parent
            var entity = Builder.Position(Position)
                                .Rotation(Rotation)
                                .Scale(Scale)
                                .Build();
            Assert.AreEqual(Position, entity.Position);
            Assert.AreEqual(Rotation, entity.Rotation);
            Assert.AreEqual(Scale, entity.Scale);
        }

        [TestMethod]
        public void TestDefaults()
        {
            var defaultPosition = Vector2.Zero;
            var defaultRotation = 0f;
            var defaultScale = Vector2.One;
            // Make the entity with no settings
            var entity = Builder.Build();
            Assert.AreEqual(defaultPosition, entity.Position);
            Assert.AreEqual(defaultRotation, entity.Rotation);
            Assert.AreEqual(defaultScale, entity.Scale);
            Assert.IsNull(entity.Parent);
        }

        [TestMethod]
        public void TestParent()
        {
            // Make a parent-child
            var parent = Builder.Position(Position)
                                .Rotation(Rotation)
                                .Scale(Scale)
                                .Build();
            // Try remove the parent from parent, nothing should happen
            parent.RemoveParent();
            Assert.IsNull(parent.Parent);
            var child = Builder.Parent(parent)
                               .RelativePosition(Vector2.Zero)
                               .RelativeRotation(0f)
                               .RelativeScale(Vector2.One)
                               .Build();
            // Check absolute transform
            Assert.AreEqual(Position, child.Position);
            Assert.AreEqual(Rotation, child.Rotation);
            Assert.AreEqual(Scale, child.Scale);
            Assert.IsNotNull(child.Parent);
            // Check relative transform
            Assert.AreEqual(Vector2.Zero, child.RelativePosition);
            Assert.AreEqual(0f, child.RelativeRotation);
            Assert.AreEqual(Vector2.One, child.RelativeScale);
            // Try remove the parent from child
            child.RemoveParent();
            Assert.IsNull(child.Parent);
        }

        [TestMethod]
        public void TestRelativeWithParent()
        {
            // Make the parent
            var parent = Builder.Position(Position)
                                .Rotation(Rotation)
                                .Scale(Scale)
                                .Build();
            // Make the child with non-default relative transform
            var child = Builder.RelativePosition(Position)
                               .RelativeRotation(Rotation)
                               .RelativeScale(Scale)
                               .Parent(parent)
                               .Build();
            Assert.AreEqual(Position, child.RelativePosition);
            Assert.AreEqual(Rotation, child.RelativeRotation);
            Assert.AreEqual(Scale, child.RelativeScale);
            Assert.AreEqual(Position * 2, child.Position);
            Assert.AreEqual(Rotation * 2, child.Rotation);
            Assert.AreEqual(Scale * 2, child.Scale);
        }

        [TestMethod]
        public void TestRelativeWithoutParent()
        {
            // Make the entity with no parent and use relative transform
            var child = Builder.RelativePosition(Position)
                               .RelativeRotation(Rotation)
                               .RelativeScale(Scale)
                               .Build();
            Assert.AreEqual(Position, child.RelativePosition);
            Assert.AreEqual(Rotation, child.RelativeRotation);
            Assert.AreEqual(Scale, child.RelativeScale);
            Assert.AreEqual(Position, child.Position);
            Assert.AreEqual(Rotation, child.Rotation);
            Assert.AreEqual(Scale, child.Scale);
        }
    }
}
