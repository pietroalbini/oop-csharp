﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TodoNDD.Entities;
using TodoNDD.Entities.Builders;

namespace TodoNDDTests
{
    [TestClass]
    public sealed class DummyEntityTest : BaseEntityTest<DummyEntityBuilder, DummyEntity>
    {
    }
}
