﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using TodoNDD.Tasks;

namespace TodoNDDTests
{
    [TestClass]
    public sealed class QueuedLoopableTaskManagerTest : BaseLoopableTaskManagerTest<QueuedLoopableTaskManager>
    {
        private const int InitialSwitchTaskID = 0;
        private const int NextSwitchTaskID = 1;
        private const int AbortTaskID = 0;
        private const int Tasks = 2;
        private const int TicksPerTask = 4;

        public QueuedLoopableTaskManagerTest() : base(Tasks, TicksPerTask)
        {
        }

        [TestMethod]
        public void TestSwitchToTask()
        {
            var result = new StringBuilder();
            // Add a task to the manager
            Manager.Add(GeneratedTasksList[InitialSwitchTaskID]);
            AppendWithID(result, 0, "I");
            // Simulate half the ticks, then switch to another task
            for (int i = 0; i < TicksPerTask / 2; i++)
            {
                Manager.Tick(1 / 60f);
                AppendWithID(result, 0, "T");
            }
            // Switch to another task
            AppendWithID(result, InitialSwitchTaskID, "D");
            Manager.SwitchToTask(GeneratedTasksList[NextSwitchTaskID]);
            AppendWithID(result, NextSwitchTaskID, "I");
            // Simulate half the ticks
            for (int i = 0; i < TicksPerTask / 2; i++)
            {
                Manager.Tick(1 / 60f);
                AppendWithID(result, 1, "T");
            }
            Assert.AreEqual(result.ToString(), OutputString);
        }

        [TestMethod]
        public void TestAbortTaskWithAnotherQueuedUp()
        {
            var result = new StringBuilder();
            // Initialize all
            AddAllTasksToManager();
            for (int i = 0; i < Tasks; i++)
            {
                AppendWithID(result, i, "I");
            }
            // Simulate half the ticks, then abort the current task
            for (int i = 0; i < TicksPerTask / 2; i++)
            {
                Manager.Tick(1 / 60f);
                AppendWithID(result, 0, "T");
            }
            Manager.AbortCurrentTask();
            AppendWithID(result, 0, "D");
            // Simulate half the ticks for the next task
            for (int i = 0; i < TicksPerTask / 2; i++)
            {
                Manager.Tick(1 / 60f);
                AppendWithID(result, 1, "T");
            }
            Assert.AreEqual(result.ToString(), OutputString);
            Assert.AreEqual(GeneratedTasksList.Count - 1, Manager.TasksCount);
        }

        [TestMethod]
        public void TestAbortTaskWithEmptyQueue()
        {
            var result = new StringBuilder();
            // Add a task to the manager
            Manager.Add(GeneratedTasksList[AbortTaskID]);
            AppendWithID(result, AbortTaskID, "I");
            // Simulate half the ticks, then abort the current task
            for (int i = 0; i < TicksPerTask / 2; i++)
            {
                Manager.Tick(1 / 60f);
                AppendWithID(result, AbortTaskID, "T");
            }
            Manager.AbortCurrentTask();
            AppendWithID(result, 0, "D");
            Assert.AreEqual(result.ToString(), OutputString);
            Assert.AreEqual(0, Manager.TasksCount);
        }

        protected override string GetExpectedMultipleTasksOutput(int tasksCount, int loops)
        {
            var result = new StringBuilder();
            // Initialize all
            for (int i = 0; i < tasksCount; i++)
            {
                AppendWithID(result, i, "I");
            }
            for (int i = 0; i < tasksCount; i++)
            {
                // Tick
                for (int j = 0; j < loops; j++)
                {
                    AppendWithID(result, i, "T");
                }
                // Destroy
                AppendWithID(result, i, "D");
            }
            return result.ToString();
        }
    }
}
