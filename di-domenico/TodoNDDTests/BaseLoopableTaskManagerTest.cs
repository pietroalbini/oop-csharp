﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TodoNDD.Tasks;

namespace TodoNDDTests
{
    [TestClass]
    public abstract class BaseLoopableTaskManagerTest<T>
        where T : ILoopableTaskManager, new()
    {
        protected T Manager
        {
            get;
        }
        protected string OutputString => this.resultBuilder.ToString();
        protected IList<ILoopableTask> GeneratedTasksList => new List<ILoopableTask>(tasks);
        private int tasksCount;
        private int loops;
        private IList<ILoopableTask> tasks;
        private StringBuilder resultBuilder;

        public BaseLoopableTaskManagerTest(int tasksCount, int loops)
        {
            Manager = new T();
            if (tasksCount <= 0 || loops <= 0)
            {
                throw new ArgumentOutOfRangeException("Invalid constructor parameters");
            }
            this.tasksCount = tasksCount;
            this.loops = loops;
        }

        [TestInitialize]
        public void Initialize()
        {
            this.resultBuilder = new StringBuilder();
            this.tasks = (from i in Enumerable.Range(0, tasksCount)
                          select new StringBuilderTask(i, loops, resultBuilder) as ILoopableTask).ToList();
        }

        [TestMethod]
        public void TestInsertion()
        {
            AddAllTasksToManager();
            // The manager must have all of them
            Assert.AreEqual(this.tasks.Count, this.Manager.TasksCount);
        }

        [TestMethod]
        public void TestDeletion()
        {
            AddAllTasksToManager();
            // Remove just one
            this.Manager.Remove(this.tasks[0]);
            Assert.AreEqual(this.tasks.Count - 1, this.Manager.TasksCount);
        }

        [TestMethod]
        public void TestSingleTask()
        {
            // Run the first task until it's over
            this.Manager.Add(this.tasks[0]);
            while (this.Manager.TasksCount > 0)
            {
                this.Manager.Tick(1 / 60f);
            }
            // The output produced by the tasks must match the one provided by the template method
            Assert.AreEqual(GetExpectedSingleTaskOutput(0, this.loops, true), this.resultBuilder.ToString());
        }

        [TestMethod]
        public void TestPartialSingleTask()
        {
            this.Manager.Add(this.tasks[0]);
            // Simulate half the ticks
            for (int i = 0; i < this.loops / 2; i++)
            {
                this.Manager.Tick(1 / 60f);
            }
            // The output produced by the tasks must match the one provided by the template method
            // with no destruction of the task
            Assert.AreEqual(GetExpectedSingleTaskOutput(0, this.loops / 2, false), this.resultBuilder.ToString());
            // The manager must still have the task
            Assert.AreEqual(1, this.Manager.TasksCount);
        }

        [TestMethod]
        public void TestMultipleTasks()
        {
            AddAllTasksToManager();
            // Run them until they're done
            while (this.Manager.TasksCount > 0)
            {
                this.Manager.Tick(1 / 60f);
            }
            Assert.AreEqual(GetExpectedMultipleTasksOutput(this.tasksCount, this.loops), this.resultBuilder.ToString());
        }

        protected string GetExpectedSingleTaskOutput(int id, int loops, bool isDone)
        {
            var result = new StringBuilder();
            // Initialize
            AppendWithID(result, id, "I");
            // Tick
            for (int i = 0; i < loops; i++)
            {
                AppendWithID(result, id, "T");
            }
            // Destroy
            if (isDone)
            {
                AppendWithID(result, id, "D");
            }
            return result.ToString();
        }

        protected abstract string GetExpectedMultipleTasksOutput(int tasksCount, int loops);

        protected void AddAllTasksToManager()
        {
            foreach (var task in this.tasks)
            {
                this.Manager.Add(task);
            }
        }

        private class StringBuilderTask : ILoopableTask
        {
            private readonly int index;
            private readonly int loops;
            private int count;
            private readonly StringBuilder resultBuilder;

            public bool IsDone => this.count >= loops;

            public StringBuilderTask(int index, int loops, StringBuilder resultBuilder)
            {
                this.index = index;
                this.loops = loops;
                this.resultBuilder = resultBuilder;
            }

            public void OnDestroy() => AppendWithID(resultBuilder, index, "D");

            public void OnInit() => AppendWithID(resultBuilder, index, "I");

            public void OnTick(float deltaTime)
            {
                AppendWithID(resultBuilder, index, "T");
                this.count++;
            }
        }

        protected static void AppendWithID(StringBuilder sb, int id, string text) => sb.Append($"{id}:{text},");
    }
}
